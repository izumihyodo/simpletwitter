package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.service.MessageService;

/**
 * Servlet implementation class EditServlet
 */

@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		HttpSession session = request.getSession();
		String judgeMessageId = request.getParameter("messageId");
		List<String> errorMessages = new ArrayList<String>();
		Message message = null;

		//	URLが空でもなく、数字の時だけ正常処理
		if (!StringUtils.isBlank(judgeMessageId) && judgeMessageId.matches("^[0-9]*$")) {
			Integer messageId = Integer.parseInt(judgeMessageId);
			message = new MessageService().select(messageId);
		}

		//存在しないつぶやき 不正なパラメーター
		if (message == null) {
			errorMessages.add("不正なパラメータが入力されました");
			session.setAttribute("errorMessages", errorMessages);
			response.sendRedirect("./");
			return;
		}
		request.setAttribute("message", message);
		request.getRequestDispatcher("edit.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<String> errorMessages = new ArrayList<String>();

		Message message = getMessage(request);

		if (!isValid(message, errorMessages)) {
				request.setAttribute("errorMessages", errorMessages);
				request.setAttribute("message", message);
				request.getRequestDispatcher("edit.jsp").forward(request, response);
				return;
		}

		new MessageService().update(message);
		//	リダイレクト
		response.sendRedirect("./");
	}

	private Message getMessage(HttpServletRequest request)
			throws IOException, ServletException {
		Message message = new Message();
		message.setId(Integer.parseInt(request.getParameter("messageId")));
		message.setText(request.getParameter("messageText"));
		return message;
	}

	private boolean isValid(Message message, List<String> errorMessages) {

		String text = message.getText();

		if (StringUtils.isBlank(text)) {
			errorMessages.add("入力してください");
		} else if (140 < text.length()) {
			errorMessages.add("140文字以下で入力してください");
		}
		if (errorMessages.size() != 0) {
			return false;
		}
		return true;
	}
}